# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('core', '0006_historylink'),
    ]

    operations = [
        migrations.CreateModel(
            name='JumpLink',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('updated', models.DateTimeField(auto_now=True)),
                ('link', models.ForeignKey(related_name='jumplink_link', blank=True, to='core.Link', null=True)),
                ('links', models.ManyToManyField(related_name='jumplink_links', null=True, to='core.Link', blank=True)),
                ('user', models.ForeignKey(related_name='jumplink_user', blank=True, to=settings.AUTH_USER_MODEL, null=True)),
            ],
            options={
                'ordering': ['-created'],
            },
            bases=(models.Model,),
        ),
        migrations.AlterModelOptions(
            name='link',
            options={'ordering': ['-created']},
        ),
        migrations.AddField(
            model_name='link',
            name='dop_link',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='link',
            name='transaction',
            field=models.PositiveIntegerField(default=1),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='historylink',
            name='link',
            field=models.ForeignKey(related_name='history_link', to='core.Link'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='historylink',
            name='user',
            field=models.ForeignKey(related_name='history_user', blank=True, to=settings.AUTH_USER_MODEL, null=True),
            preserve_default=True,
        ),
    ]
