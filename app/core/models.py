# -*- coding: utf-8 -*-
import hashlib
import random
from urlparse import urlparse
from django.db import models
from django.db.models.signals import post_save
import time
from accounts import UserProfile
import urllib2
import lxml.html


def _createLink():
    """
    Function for creating 6 symbols random hash for referral code
    """
    hash = hashlib.md5()
    hash.update(str(time.time()))
    randoms = ''.join(random.sample(hash.hexdigest()[:29], 2)) +\
              ''.join(random.sample('ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz123456789', 3))
    return randoms

#Link
class Link(models.Model):
    user = models.ForeignKey(UserProfile, related_name="link_user", blank=True, null=True)
    site_name = models.CharField(max_length=1000, default='', blank=True, verbose_name='Site name')
    site = models.CharField(max_length=1000, default='', blank=True, verbose_name='Site url')
    slug = models.CharField(max_length=6, unique=True, db_index=True, verbose_name='Shooter url')#shooter url
    absolute_url = models.CharField(max_length=3000, default='', blank=True, verbose_name='Absolute url')

    seo_title = models.CharField(max_length=3000, default='', blank=True, verbose_name='Seo Title(Site title)')
    seo_description = models.CharField(max_length=2000, default='', blank=True, verbose_name='Seo description')
    seo_keywords = models.CharField(max_length=2000, default='', blank=True, verbose_name='Seo keywords')
    seo_image = models.CharField(max_length=1950, default='', blank=True, verbose_name='Seo image')

    dop_link = models.BooleanField(default=False)
    transaction = models.PositiveIntegerField(default=1)

    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ['-created']

    @property
    def views(self):
        return self.history_link.count()

    def viewst(self):
        return self.history_link.count()

    #главная ссылка источник перехода
    def source(self):
        jump = JumpLink.objects.get(links=self).link
        link = Link.objects.get(id=jump.id)
        site_one = 'http://wordzam.com/'
        href = '<a href="' + site_one + link.slug + '">' + link.seo_title + '</a>'
        return href

#histri
class HistoryLink(models.Model):
    user = models.ForeignKey(UserProfile, related_name="history_user", blank=True, null=True)
    link = models.ForeignKey(Link, related_name='history_link')

    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)


#Link
class DopLink(models.Model):
    user = models.ForeignKey(UserProfile, related_name="doplink_user", blank=True, null=True)
    site_name = models.CharField(max_length=1000, default='', blank=True, verbose_name='Site name')
    site = models.CharField(max_length=1000, default='', blank=True, verbose_name='Site url')
    slug = models.CharField(max_length=6, unique=True, db_index=True, verbose_name='Shooter url')#shooter url
    absolute_url = models.CharField(max_length=3000, default='', blank=True, verbose_name='Absolute url')

    seo_title = models.CharField(max_length=3000, default='', blank=True, verbose_name='Seo Title(Site title)')
    seo_description = models.CharField(max_length=2000, default='', blank=True, verbose_name='Seo description')
    seo_keywords = models.CharField(max_length=2000, default='', blank=True, verbose_name='Seo keywords')
    seo_image = models.CharField(max_length=1950, default='', blank=True, verbose_name='Seo image')

    dop_link = models.BooleanField(default=True)
    transaction = models.PositiveIntegerField(default=1)

    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ['-created']

#переходы с основных линков
class JumpLink(models.Model):
    link = models.ForeignKey(Link, related_name="jumplink_link", blank=True, null=True)
    user = models.ForeignKey(UserProfile, related_name="jumplink_user", blank=True, null=True)
    links = models.ManyToManyField(Link, related_name="jumplink_links", blank=True, null=True)
    dop_links = models.ManyToManyField(DopLink, related_name="jumplink_dop_links", blank=True, null=True)
    porcent = models.PositiveIntegerField(default=50,blank=True,null=True)
    """
    site_name = models.CharField(max_length=1000, default='', blank=True, verbose_name='Site name')
    site = models.CharField(max_length=1000, default='', blank=True, verbose_name='Site url')
    slug = models.CharField(max_length=7, unique=True, db_index=True, verbose_name='Shooter url')#shooter url
    absolute_url = models.CharField(max_length=3000, default='', blank=True, verbose_name='Absolute url')

    seo_title = models.CharField(max_length=3000, default='', blank=True, verbose_name='Seo Title(Site title)')
    seo_description = models.CharField(max_length=251, default='', blank=True, verbose_name='Seo description')
    seo_keywords = models.CharField(max_length=251, default='', blank=True, verbose_name='Seo keywords')
    seo_image = models.CharField(max_length=1950, default='', blank=True, verbose_name='Seo image')
    """

    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ['-created']

    #общее количество переходов
    def tra_total(self):
        total = 0
        for s in self.links.all():
            total += s.transaction
        return total


#Обновляем просмотры
def update_views_stock(sender, instance, **kwargs):
    if kwargs["created"]:
      try:
        link_id = instance.link.id
        jump = JumpLink.objects.get(link=link_id)
        porcent = jump.porcent
        views = jump.link.viewst()
        if views == 1:
            pass
            #update_dop_links(jump.link.id)
        elif views > 6:
            total = views*porcent/100
            tra_total = jump.tra_total()
            if total > tra_total:
                  links = [s.id for  s in jump.dop_links.filter(seo_title__isnull=False).order_by('seo_title').distinct('seo_title')]
                  print(links)
                  dop = DopLink.objects.get(id=random.choice(links))

                  short = _createLink()
                  id = fast_url_dot(dop.absolute_url, short)
                  tran = random.randint(0,5)
                  if tran == 0:
                      tran = 1
                  Link.objects.filter(id=id).update(transaction=tran, seo_title=dop.seo_title,user=jump.user,dop_link=True,
                       seo_description=dop.seo_description, seo_keywords=dop.seo_keywords, seo_image=dop.seo_image)
                  jump.links.add(Link.objects.get(id=id))
                  jump.save()
      except:
          pass

#Быстрое создание parser Link
def fast_url_dot(urly, short):
    try:
        obj = Link.objects.create(site_name='', site='', slug=short, absolute_url=urly, dop_link=True)
        return obj.id
    except:
        return fast_url_dot(urly, short)

post_save.connect(update_views_stock, sender=HistoryLink, dispatch_uid="update_views_stock")


#Добавляем доп. ссылки
def update_dop_links(id):
        url = Link.objects.get(id=id)
        #req = urllib2.Request(url.absolute_url, headers=hdr)
        urls = urllib2.build_opener(urllib2.HTTPCookieProcessor).open(url.absolute_url)

        try:
            hostname = urlparse(url.absolute_url).hostname
            scheme =  urlparse(url.absolute_url).scheme
        except:
            hostname = None
            scheme =  None
        tree = lxml.html.parse(urls).getroot()
        tiles = tree.find(".//title")
        if tiles:
            title = tree.find(".//title").text
        else:
            try:
               title = tree.cssselect('head title')[0].text
            except:
                title = ''
        zet = tree.cssselect('a')

        links = []
        for z in zet:
            #urllib2.build_opener(urllib2.HTTPCookieProcessor).open('http/www.championat.com/football/news-2086136-ibragimovich-povtoril-rekord-davidsa-po-chislu-krasnykh-kartochek-v-lige-chempionov.html')
            print(z.get('href'))
            try:
               sss = str(scheme) +'://' + str(hostname) + str(z.get('href'))
               #lins = urllib2.build_opener(urllib2.HTTPCookieProcessor).open(sss)
               #trees = lxml.html.parse(lins).getroot()
               #print(.text)
               links.append(sss)
            except:
               pass
        print(777, links)

        for lin in links:
            try:
                 lins = urllib2.build_opener(urllib2.HTTPCookieProcessor).open(lin)
                 tree = lxml.html.parse(lins).getroot()
                 tiles = tree.find(".//title")
                 if tiles:
                      title = tree.find(".//title").text
                 else:
                      try:
                           title = tree.cssselect('head title')[0].text
                      except:
                           title = ''
                 description = ''.join(tree.xpath("/html/head/meta[@name='description']/@content"))
                 keywords = ''.join(tree.xpath("/html/head/meta[@name='keywords']/@content"))
                 image = ''.join(tree.xpath("/html/head/meta[@property='og:image']/@content"))
                 favicon = ''.join(tree.xpath('//link[@rel="shortcut icon"]/@href'))
                 if title and description:
                   if len(title) > 22 and len(description) > 38:
                     short = _createLink()
                     usr = url.user
                     if not usr:
                         usr= None
                     respo = fast_url_parser(lin, short)
                     s = DopLink.objects.filter(slug=short).update(seo_title=title,user=usr,dop_link=True,
                       seo_description=description, seo_keywords=keywords, seo_image=image)
                     res = DopLink.objects.get(slug=short)
                     jumps = JumpLink.objects.get(link=url)
                     jumps.dop_links.add(res)
                     jumps.save()
            except:
                 pass

#Быстрое создание parser DopLink
def fast_url_parser(urly, short):
    try:
        obj = DopLink.objects.create(site_name='', site='', slug=short, absolute_url=urly, dop_link=True)
        id = obj.id
        return True, id
    except:
        return fast_url_parser(urly, short)