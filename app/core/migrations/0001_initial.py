# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Link',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('site_name', models.CharField(default=b'', max_length=120, verbose_name=b'Site name', blank=True)),
                ('site', models.CharField(default=b'', max_length=120, verbose_name=b'Site url', blank=True)),
                ('slug', models.CharField(unique=True, max_length=6, verbose_name=b'Shooter url', db_index=True)),
                ('absolute_url', models.CharField(default=b'', max_length=2000, verbose_name=b'Absolute url', blank=True)),
                ('seo_title', models.CharField(default=b'', max_length=100, verbose_name=b'Seo Title(Site title)', blank=True)),
                ('seo_description', models.CharField(default=b'', max_length=251, verbose_name=b'Seo description', blank=True)),
                ('seo_keywords', models.CharField(default=b'', max_length=251, verbose_name=b'Seo keywords', blank=True)),
                ('seo_image', models.CharField(default=b'', max_length=950, verbose_name=b'Seo image', blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
