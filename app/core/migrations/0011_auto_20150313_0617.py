# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0010_jumplink_porcent'),
    ]

    operations = [
        migrations.AlterField(
            model_name='doplink',
            name='seo_description',
            field=models.CharField(default=b'', max_length=2000, verbose_name=b'Seo description', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='doplink',
            name='seo_keywords',
            field=models.CharField(default=b'', max_length=2000, verbose_name=b'Seo keywords', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='link',
            name='seo_description',
            field=models.CharField(default=b'', max_length=2000, verbose_name=b'Seo description', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='link',
            name='seo_keywords',
            field=models.CharField(default=b'', max_length=2000, verbose_name=b'Seo keywords', blank=True),
            preserve_default=True,
        ),
    ]
