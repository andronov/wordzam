from __future__ import absolute_import

from celery import shared_task, task
from django.dispatch import receiver
from django.db.models import signals
import time
from core.models import Link
from accounts.models import  UserProfile
from django.db.models.signals import post_save




def on_custom_feed_save(sender, instance, **kwargs):
    if kwargs["created"]:
         name = 'test'


         routing_key = 'tasks.{0}'.format(1)
         lazy_job.apply_async(args=[name], routing_key=routing_key)
         update_dop_links.apply_async(args=[instance.id], routing_key=routing_key)


post_save.connect(on_custom_feed_save, sender=Link, dispatch_uid="on_link_save")

@task(ignore_result=True)
def update_dop_links(id):
    result = Link.objects.filter(id=id)
    s= UserProfile.objects.get(id=1)
    s.username = 'johned'
    s.save()

    print(result)

@task(ignore_result=True)
def lazy_job(name):
    result = Link.objects.filter(id=1)
    s= UserProfile.objects.get(id=1)
    s.username = 'johned'
    s.save()
    logger = lazy_job.get_logger()
    logger.info('Starting the lazy job: {0}'.format(name))
    time.sleep(5)
    logger.info('Lazy job {0} completed'.format(name))
