# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0009_auto_20150313_0121'),
    ]

    operations = [
        migrations.AddField(
            model_name='jumplink',
            name='porcent',
            field=models.PositiveIntegerField(default=50, null=True, blank=True),
            preserve_default=True,
        ),
    ]
