# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0002_link_user'),
    ]

    operations = [
        migrations.AlterField(
            model_name='link',
            name='seo_title',
            field=models.CharField(default=b'', max_length=2000, verbose_name=b'Seo Title(Site title)', blank=True),
            preserve_default=True,
        ),
    ]
