# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0008_auto_20150313_0113'),
    ]

    operations = [
        migrations.AlterField(
            model_name='jumplink',
            name='dop_links',
            field=models.ManyToManyField(related_name='jumplink_dop_links', null=True, to='core.DopLink', blank=True),
            preserve_default=True,
        ),
    ]
