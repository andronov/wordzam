# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('core', '0007_auto_20150312_0401'),
    ]

    operations = [
        migrations.CreateModel(
            name='DopLink',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('site_name', models.CharField(default=b'', max_length=1000, verbose_name=b'Site name', blank=True)),
                ('site', models.CharField(default=b'', max_length=1000, verbose_name=b'Site url', blank=True)),
                ('slug', models.CharField(unique=True, max_length=6, verbose_name=b'Shooter url', db_index=True)),
                ('absolute_url', models.CharField(default=b'', max_length=3000, verbose_name=b'Absolute url', blank=True)),
                ('seo_title', models.CharField(default=b'', max_length=3000, verbose_name=b'Seo Title(Site title)', blank=True)),
                ('seo_description', models.CharField(default=b'', max_length=251, verbose_name=b'Seo description', blank=True)),
                ('seo_keywords', models.CharField(default=b'', max_length=251, verbose_name=b'Seo keywords', blank=True)),
                ('seo_image', models.CharField(default=b'', max_length=1950, verbose_name=b'Seo image', blank=True)),
                ('dop_link', models.BooleanField(default=True)),
                ('transaction', models.PositiveIntegerField(default=1)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('updated', models.DateTimeField(auto_now=True)),
                ('user', models.ForeignKey(related_name='doplink_user', blank=True, to=settings.AUTH_USER_MODEL, null=True)),
            ],
            options={
                'ordering': ['-created'],
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='jumplink',
            name='dop_links',
            field=models.ManyToManyField(related_name='jumplink_dop_links', null=True, to='core.Link', blank=True),
            preserve_default=True,
        ),
    ]
