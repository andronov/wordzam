# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0003_auto_20150219_2301'),
    ]

    operations = [
        migrations.AlterField(
            model_name='link',
            name='absolute_url',
            field=models.CharField(default=b'', max_length=3000, verbose_name=b'Absolute url', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='link',
            name='seo_image',
            field=models.CharField(default=b'', max_length=1950, verbose_name=b'Seo image', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='link',
            name='seo_title',
            field=models.CharField(default=b'', max_length=3000, verbose_name=b'Seo Title(Site title)', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='link',
            name='site',
            field=models.CharField(default=b'', max_length=1000, verbose_name=b'Site url', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='link',
            name='site_name',
            field=models.CharField(default=b'', max_length=1000, verbose_name=b'Site name', blank=True),
            preserve_default=True,
        ),
    ]
