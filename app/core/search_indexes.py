import datetime
from haystack import indexes
from core.models import Link


class TweetIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True,template_name="search/indexes/core/link_text.txt")
    seo_title = indexes.CharField(model_attr='seo_title', faceted=True)
    seo_keywords = indexes.CharField(model_attr='seo_keywords', faceted=True)
    seo_description = indexes.CharField(model_attr='seo_description', faceted=True)

    seo_title_auto = indexes.EdgeNgramField(model_attr='seo_title')
    seo_description_auto = indexes.EdgeNgramField(model_attr='seo_description')
    seo_keywords_auto = indexes.EdgeNgramField(model_attr='seo_keywords')

    def get_model(self):
        return Link

    def index_queryset(self, using=None):
        """Used when the entire index for model is updated."""
        return self.get_model().objects.all()
