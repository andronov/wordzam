function updatedom(){
  var client = new ZeroClipboard( document.getElementById("copy-button") );
  client.on( "ready", function( readyEvent ) {

  client.on( "aftercopy", function( event ) {
    // `this` === `client`
    // `event.target` === the element that was clicked
    event.target.style.color = "white";
    event.target.style.background = "rgb(175, 30, 45)";

    //alert("Copied text to clipboard: " + event.data["text/plain"] );
    });
  });
}

//add content link
function slowaddcontent(data){
     var text = '<div class="socials">'+
                '<a onclick="window.open(this.href,this.target,\'width=900,height=600\');return false;" class="soc-menu__icon_fb_big" id="fb" href="http://www.facebook.com/sharer/sharer.php?u=http://wordzam.com/'+data['data']['short']+'"></a>'+
                '<a onclick="window.open(this.href,this.target,\'width=600,height=300\');return false;" class="soc-menu__icon_tw_big" id="tw" href="http://twitter.com/intent/tweet?url=http://wordzam.com/'+data['data']['short']+'&amp;text='+data['data']['desc']+'"></a>'+
            '</div><div style="clear:both"></div>';
  $('#shooter_'+data['data']['id']+'').append(text);
  $('#shooter_'+data['data']['id']).append(data['data']['html']);
}

function slowurl(short) {
    var csrf = $("input[name='csrfmiddlewaretoken']").val();
    $.ajax({
                url: '/slow',
                data: {'csrfmiddlewaretoken':csrf, 'short':short},
                method:'post',
                success: function(data) {
                  slowaddcontent(data)
                }
     });
}

function doplinks(short) {
    var csrf = $("input[name='csrfmiddlewaretoken']").val();
    $.ajax({
                url: '/doplink',
                data: {'csrfmiddlewaretoken':csrf, 'short':short},
                method:'post',
                success: function(data) {

                }
     });
}

function showalert(message,alerttype) {
    $('.alert-danger').html(message).fadeIn('slow');
    setTimeout(function() {$(".alert-danger").fadeOut();}, 3000);
}

var actionButtonsCallbacks = function () {
  return {
    click: {
      //Default click callback
      defaultCallback: function(e, trigger) {
        alert('default');
      }
    },
    submit: {
      //Default submit callback
      defaultCallback: function(e, form, data) {
        var trigger = form.find('.submit-trigger');
        trigger.on('hidden.bs.tooltip', function () { trigger.tooltip('destroy'); });
        trigger.tooltip({title: 'Сохранено', trigger: 'manual', html: true, placement: 'left'}).tooltip('show');
        setTimeout(function(){ trigger.tooltip('hide'); }, 3000);
      },
      shortWordForm: function(e, form, data) {
        form.find('.alert-danger').remove();
        console.log(1, data);

        if(data['data']['short']) {
            $('#result-short').prepend('<div data-id="'+data['data']['id']+'" id="shooter_'+data['data']['id']+'" class="shooter"><h2 class="shooterh2">' +
            '<a target="_blank" class="link-short"  href="/'+data['data']['short']+'">http://wordzam.com/'+data['data']['short']+'</a>' +
            '<a id="copy-button" class="copy-buttons" data-clipboard-text="http://wordzam.com/'+data['data']['short']+'" title="Click to copy.">Copy</a>' +
            '</h2></div>');
            slowurl(data['data']['short']);
            doplinks(data['data']['short']);
            updatedom()
        }
        else if(data['data']['error']) {
            showalert(data['data']['error'],'short');
            //$('.navdrawer-container').after('<div class="alert alert-danger fade in">'+ data['data']['error'] +'<a class="close" data-dismiss="alert" href="#">&times;</a></div>')
        } else {
            //$('.navdrawer-container').after('<div class="alert alert-success fade in">'+ data['data']['success'] +'<a class="close" data-dismiss="alert" href="#">&times;</a></div>')
        }
      }
    },
    validate: {
        //Default validate callback. Return TRUE if there is some errors
        defaultCallback: function (e, form, values) {
            return false;
        }
    },
    ajax: {
      //Default AJAX callback
      defaultCallback: function(e, trigger, data) {
        alert('AJAX default');
        console.log(e, trigger, data);
      },
      //
      FilterDateCallback: function(e, trigger, data) {
          RefreshTime();
          var replacement = $(data);
          var row = $(document).find('.line-tbody');
          row.replaceWith(data['data']['html']);
      }
    }
  };
}();

(function($) {

  $.extend($.fn, {
    actionButton: function(){
      $(this).each(function(){
        var trigger = $(this);
        if(!trigger.hasClass('action-processed')) {
          var url = trigger.attr('href') && trigger.attr('href') != '#' ? trigger.attr('href') : trigger.data('href'), callback = actionButtonsCallbacks.click.defaultCallback, callbackText = trigger.data("callback"),
              method = trigger.data('method') ? trigger.data('method').toLowerCase() : 'post',
              ajaxing_cont = trigger.data('ajaxcont') ? $(trigger.data('ajaxcont')) : trigger;
          if(url && url != '#' && !trigger.data('noajax')) {
            //ajaxCallback
            callback = callbackText && $.isFunction(actionButtonsCallbacks.ajax[callbackText]) ? actionButtonsCallbacks.ajax[callbackText] : actionButtonsCallbacks.ajax.defaultCallback;
            var csrf = $("input[name='csrfmiddlewaretoken']").val();
            trigger.click(function(e) {
              ajaxing_cont.addClass('ajaxing');
              e.preventDefault();
              $.ajax({
                url: url,
                data: {'csrfmiddlewaretoken':csrf},
                method:'post',
                success: function(data) {
                  //ajaxing_cont.removeClass('ajaxing').addClass('ajaxingOut');
                  //setTimeout(function(){ ajaxing_cont.removeClass('ajaxingOut'); }, 400);
                  //Trigger success callback
                  callback(e, trigger, data);
                }
              });
            });

          } else {
            //simpleCallback
            callback = callbackText && $.isFunction(actionButtonsCallbacks.click[callbackText]) ? actionButtonsCallbacks.click[callbackText] : actionButtonsCallbacks.click.defaultCallback;
            trigger.click(function(e) {
              e.preventDefault();
              callback(e, trigger);
            });
          }
          trigger.addClass('action-processed');
        }
      });

      return this;
    },
    //Good serialization of form for AJAX
    serializeFormToObject: function(){
      var o = {};
      var a = this.serializeArray();
      $.each(a, function() {
        if (o[this.name] !== undefined) {
          if (!o[this.name].push) {
              o[this.name] = [o[this.name]];
          }
          o[this.name].push(this.value || '');
        } else {
          o[this.name] = this.value || '';
        }
      });
      return o;
    },
    //AJAX Form
    ajaxifyForm: function(){
      $(this).each(function(){
        var form = $(this);
        if(!form.hasClass('ajax-processed')) {
          form.submit(function(e) {
            e.preventDefault();
            var submitCallback = form.data("submit") && $.isFunction(actionButtonsCallbacks.submit[form.data("submit")]) ? actionButtonsCallbacks.submit[form.data("submit")] : actionButtonsCallbacks.submit.defaultCallback,
                validateCallback = form.data("validate") && $.isFunction(actionButtonsCallbacks.validate[form.data("validate")]) ? actionButtonsCallbacks.validate[form.data("validate")] : actionButtonsCallbacks.validate.defaultCallback,
                errors = false, values = form.serializeFormToObject(),
                url = form.attr('action') ?  form.attr('action') : window.location.href,
                method = form.attr('method') ? form.attr('method').toLowerCase() : 'get';
            //Check form for errors
            errors = validateCallback(e, form,  values);
            //If NO ERRORS than SEND form
            if(!errors) {
              $.ajax({
                url: url,
                data: values,
                method: method,
                success: function(data) {
                  //Trigger submit callback
                  submitCallback(e, form, data);
                }
              });
            }
          });
          form.addClass('ajax-processed');
        }
      });
      return this;
    }
  });

  //Custom event for AJAX loaded content
  $(document).on('contentAdded', function(e, data){
    var target = data.target;
    //Action Buttons
    target.find('.action-button').actionButton();
    //AJAX forms
    target.find('.ajaxify-form').ajaxifyForm();
  });



  $(document).ready(function(){
    //Event trigget on DOM ready
    $(this).trigger('contentAdded', {target: $('html')});
  });

})(jQuery);