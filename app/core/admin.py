from django.contrib import admin

# Register your models here.
from core.models import *

class LinkAdmin(admin.ModelAdmin):
    model = Link

    prepopulated_fields = {"slug": ('slug',)}
    list_display = ['slug','user_name',  'absolute_url', 'seo_title', 'seo_description',
                    'dop_link', 'transaction', 'created']
    #search_fields = ('team_one_name', )

    def user_name(self, instance):
        return instance.user

    user_name.short_description = 'User'

class DopLinkAdmin(admin.ModelAdmin):
    model = DopLink

    prepopulated_fields = {"slug": ('slug',)}
    list_display = ['slug','user_name',  'absolute_url', 'seo_title', 'seo_description',
                    'dop_link', 'transaction', 'created']
    #search_fields = ('team_one_name', )

    def user_name(self, instance):
        return instance.user

    user_name.short_description = 'User'


admin.site.register(DopLink, DopLinkAdmin)
admin.site.register(Link, LinkAdmin)
admin.site.register(HistoryLink)
admin.site.register(JumpLink)