from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.contrib.staticfiles import views
from core.views import *

urlpatterns = patterns('',
    # Examples:
    url(r'^$', HomePageView.as_view(), name='core-home'),
    url(r'^short', ShortPageView.as_view(), name='core-short'),
    url(r'^feed', UserFeedView.as_view(), name='core-feed'),
    url(r'^views', ViewsFeedView.as_view(), name='core-views'),
    url(r'^slow', slow_url, name='slow-url'),
    #url(r'^$', 'core.views.test', name='test'),
    # url(r'^blog/', include('blog.urls')),
    #(r'^search', include('haystack.urls')),
    url(r'^doplink', update_dop_link, name='doplink'),

    (r'^grappelli/', include('grappelli.urls')), # grappelli URLS
    url(r'^admin', include(admin.site.urls)),

    #url(r'^example', example, name='example'),

    url('', include('social.apps.django_app.urls', namespace='social')),
    #url(r'^$', 'accounts.custom_views.login'),
    url(r'^home/$', 'accounts.custom_views.home'),
    url(r'^logout$', 'accounts.custom_views.logout'),

    #related detail
    url(r'^r/(?P<slug>[0-9A-Za-z_\-]+)', RelatedView.as_view(), name='core-related'),
    #iframe detail
    url(r'^(?P<slug>[0-9A-Za-z_\-]+)', LinkDetailView.as_view(), name='core-iframe'),
)
urlpatterns += [
        url(r'^static/(?P<path>.*)$', views.serve),
]