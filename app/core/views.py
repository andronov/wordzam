# -*- coding: utf-8 -*-
import hashlib
import json
import urllib2, cookielib
import time
import random
from urlparse import urlparse
import datetime
from django.db.models import Q, Count, Sum
from django.http import HttpResponse
from django.shortcuts import redirect
from django.template import RequestContext
from django.template.loader import render_to_string
from django.views.generic import TemplateView, DetailView, ListView
from haystack.inputs import Clean, AutoQuery, Raw
from haystack.query import SearchQuerySet
import lxml.html

#Random url
from accounts import UserProfile
from core.models import Link, HistoryLink, JumpLink, DopLink
from core.search_indexes import TweetIndex

hdr = {'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.115 Safari/537.36',
       'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
       'Accept-Language:' : 'ru-RU,ru;q=0.8,en-US;q=0.6,en;q=0.4',
       'Accept-Charset': 'ISO-8859-1,utf-8;q=0.7,*;q=0.3',
       'Accept-Encoding': 'gzip, deflate, sdch',
       'Accept-Language': 'en-US,en;q=0.8',
       'Connection': 'keep-alive'}

def _createLink():
    """
    Function for creating 6 symbols random hash for referral code
    """
    hash = hashlib.md5()
    hash.update(str(time.time()))
    randoms = ''.join(random.sample(hash.hexdigest()[:29], 2)) +\
              ''.join(random.sample('ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz123456789', 3))
    return randoms

class HomePageView(TemplateView):
    template_name = 'core/home.html'

    def get_context_data(self, **kwargs):
        context = super(HomePageView, self).get_context_data(**kwargs)
        time_start_dt = datetime.datetime.now() - datetime.timedelta(days=1)
        time_finish_dt= datetime.datetime.now()
        qss = Link.objects.filter(created__range=(time_start_dt, time_finish_dt)).order_by('seo_title').distinct('seo_title')
        p = [s.id for s in qss]
        qs = Link.objects.filter(id__in=p,dop_link=False,seo_title__isnull=False,seo_description__isnull=False, ).annotate(score=Count('history_link')).order_by('-score')
        print(qs)
        s = qs[:5]
        g = sorted(Link.objects.filter(id__in=p),  key=lambda m: m.views)
        context['object_list'] = s
        return context

    def render_to_response(self, context, **response_kwargs):
        response = super(HomePageView, self).render_to_response(context, **response_kwargs)
        return response

class ShortPageView(TemplateView):
    template_name = 'core/short.html'

    def get_context_data(self, **kwargs):
        context = super(ShortPageView, self).get_context_data(**kwargs)
        return context

    def post(self, request, *args, **kwargs ):
        context = self.get_context_data()
        data = {}
        try:
            req = urllib2.Request(request.POST['short'], headers=hdr)
            urllib2.build_opener(urllib2.HTTPCookieProcessor).open(req)
            short = _createLink()
            respo = fast_url(request.POST['short'], short)
            if not respo:
                short = _createLink()
                fast_url(request.POST['short'], short)
            data['short'] = short
            data['id'] = Link.objects.get(slug=short).id
        except:
            data['error'] = u'Unable to shorten that link. It is not a valid url.'

        response = HttpResponse(json.dumps({'data': data}), content_type="application/json")
        return response

    def render_to_response(self, context, **response_kwargs):
        response = super(ShortPageView, self).render_to_response(context, **response_kwargs)
        return response

#Лента новостей юзера
class UserFeedView(ListView):
    template_name = 'core/feed-user-list.html'
    queryset = Link.objects.all()

    def get_queryset(self):
        qs = super(UserFeedView, self).get_queryset()
        #qs = qs.filter(user=self.request.user)
        results = SearchQuerySet().filter(content=AutoQuery(result_keywords(self.request.user.id)))
        ids = [result.object.id for result in results]
        qss = qs.filter(id__in=ids).order_by('seo_title').distinct('seo_title')
        p = [s.id for s in qss]
        qs = qs.filter(id__in=p).order_by('-created')[:60]
        return qs

    def dispatch(self, *args, **kwargs):
        if not self.request.user.is_authenticated():
                return redirect('auth_login')
        return super(UserFeedView, self).dispatch(*args, **kwargs)

#Лента просмотров юзера
class ViewsFeedView(ListView):
    template_name = 'core/views-user-list.html'
    queryset = Link.objects.filter(dop_link=True)

    def get_queryset(self):
        qs = super(ViewsFeedView, self).get_queryset()

        jump = JumpLink.objects.filter(user=self.request.user)

        qss = qs.filter(user=self.request.user).order_by('seo_title').distinct('seo_title')
        p = [s.id for s in qss]
        qs = qs.filter(id__in=p).order_by('-created')[:60]
        return qs

    def dispatch(self, *args, **kwargs):
        if not self.request.user.is_authenticated():
                return redirect('auth_login')
        return super(ViewsFeedView, self).dispatch(*args, **kwargs)

#Быстрое создание
def fast_url(urly, short):
    try:
        obj = Link.objects.create(site_name='', site='', slug=short, absolute_url=urly)
        id = obj.id
        return True, id
    except:
        return fast_url(urly, short)

#после ссылки
def slow_url(request):
    context = RequestContext(request)
    data = {}
    if 'short' in request.POST:
        user = None
        if request.user.is_authenticated():
            user = UserProfile.objects.get(id=request.user.id)
        short = request.POST['short']
        url = Link.objects.get(slug=short)
        req = urllib2.Request(request.POST['short'], headers=hdr)
        urls = urllib2.build_opener(urllib2.HTTPCookieProcessor).open(url.absolute_url)
        tree = lxml.html.parse(urls).getroot()
        tiles = tree.find(".//title")
        if tiles:
            title = tree.find(".//title").text
        else:
            try:
               title = tree.cssselect('head title')[0].text
            except:
                title = ''
        description = ''.join(tree.xpath("/html/head/meta[@name='description']/@content"))
        keywords = ''.join(tree.xpath("/html/head/meta[@name='keywords']/@content"))
        image = ''.join(tree.xpath("/html/head/meta[@property='og:image']/@content"))
        favicon = ''.join(tree.xpath('//link[@rel="shortcut icon"]/@href'))
        Link.objects.filter(slug=short).update(seo_title=title,user=user,
                       seo_description=description, seo_keywords=keywords, seo_image=image)
        s = Link.objects.get(slug=short)


        procent = random.randint(50,80)
        JumpLink.objects.create(link=s,user=user,porcent=procent)
        TweetIndex().update_object(s)
        #r = urllib2.build_opener(urllib2.HTTPCookieProcessor)
        #r.open('http://wordzam.com/' + s.slug)
        #r.close()
        context['obj'] = s
        data['id'] = url.id
        data['desc'] = title
        data['short'] = url.slug
        data['html'] = render_to_string("core/slow-url.html", context)

    response = HttpResponse(json.dumps({'data': data}), content_type="application/json")
    return response

def split_by_words(st):
    #word can be either delimeted by spaces or be put in double quotes
    #escape sequences should be also taken into account
    wordlist = []
    sepatators = [',']
    quotes = ['"']
    escapes = ['\\']
    state = dict(in_none = 0, in_word=1, in_quotes=2)
    escape_state = False
    current_word = ' '
    current_state = state['in_none']

    for index, char in enumerate(st):
        if current_state == state['in_none']:
            if char in quotes:
                current_state = state['in_quotes']
            elif char in escapes:
                escape_state = True
                current_state = state['in_word']
            elif char in sepatators:
                current_state = state['in_none']
            else:
                current_state = state['in_word']
        elif current_state == state['in_word']:
            if escape_state:
                escape_state = False
            else:
                if char in sepatators:
                    wordlist.append(current_word)
                    current_word = ''
                    current_state = state['in_none']
                elif char in quotes:
                    wordlist.append(current_word)
                    current_word = ''
                    current_state = state['in_quotes']
                elif char in escapes:
                    escape_state = True
        elif current_state == state['in_quotes']:
            if escape_state:
                escape_state = False
            else:
                if char in quotes:
                    current_word += char
                    wordlist.append(current_word)
                    current_word = ''
                    current_state = state['in_none']
                elif char in escapes:
                    escape_state = True

        if current_state != state['in_none']:
            current_word += char
            if index == len(st)-1 and current_word:
                wordlist.append(current_word)
    str = ""
    for key in wordlist[0:3]:
        str += key
    return str

def obrezka(user_id,id):
    link = Link.objects.get(id=id)
    user = UserProfile.objects.get(id=user_id)
    if link.seo_keywords:
        k = link.seo_keywords
        userkeyss = user.keywords
        userkeys = userkeyss[0:350]
        keys = split_by_words(k)
        d = userkeys.split()
        also = ' '.join(d)
        res = keys + ' ' + also
        user.keywords = res[0:350]
        user.save()

def result_keywords(user_id):
    user = UserProfile.objects.get(id=user_id)
    return user.keywords




"""
DETAIL VIEW
"""
class LinkDetailView(DetailView):
    model = Link
    template_name = 'core/iframe-detail.html'

    def get_queryset(self):
        qs = super(LinkDetailView, self).get_queryset()
        return qs

    def get_context_data(self, **kwargs):
        context = super(LinkDetailView, self).get_context_data(**kwargs)
        user = None
        if self.request.user.is_authenticated():
            user = self.request.user
            obrezka(user.id,self.get_object().id)
        HistoryLink.objects.create(user=user, link=self.get_object())
        return context

    def render_to_response(self, context, **response_kwargs):
        response = super(LinkDetailView, self).render_to_response(context, **response_kwargs)
        return response

"""
RELATED VIEW
"""
class RelatedView(ListView):
    template_name = 'core/related-list.html'
    slug_field = 'slug'
    slug_url_kwarg = 'slug'
    queryset = Link.objects.all()


    def get_queryset(self):
        qs = super(RelatedView, self).get_queryset()
        slug = self.kwargs.get(self.slug_url_kwarg, None)
        link = Link.objects.get(slug=slug)
        text = link.seo_title + ' ' + link.seo_description + ' ' + link.seo_keywords
        if link.seo_title:
           results = SearchQuerySet().filter(content=AutoQuery(link.seo_title))
        elif link.seo_description:
           results = SearchQuerySet().filter(content=AutoQuery(link.seo_description))
        elif link.seo_keywords:
           results = SearchQuerySet().filter(content=AutoQuery(link.seo_keywords))
        else:
            results = None
        ids = [result.object.id for result in results]
        qss = qs.filter(id__in=ids).order_by('seo_title').distinct('seo_title')
        p = [s.id for s in qss]
        qs = qs.exclude(id=link.id,seo_title=link.seo_title).filter(id__in=p).order_by('-created')[:50]
        return qs

#обновляет просмотры
def update_views():
    """
    jump = JumpLink.objects.get(id=13)
    porcent = jump.porcent
    views = jump.link.views()
    if views > 5:
        total = views*porcent/100
        tra_total = jump.tra_total()
        if total > tra_total:
            links = [s.id for  s in jump.dop_links.filter(user__isnull=False, seo_title__isnull=False).order_by('seo_title').distinct('seo_title')]
            dop = DopLink.objects.get(id=random.choice(links))
            short = _createLink()
            id = fast_url_dot(dop.absolute_url, short)
            tran = random.randint(0,5)
            if tran == 0:tran = 1
            Link.objects.filter(id=id).update(transaction=tran, seo_title=dop.seo_title,user=dop.user,dop_link=True,
                       seo_description=dop.seo_description, seo_keywords=dop.seo_keywords, seo_image=dop.seo_image)
            jump.links.add(Link.objects.get(id=id))
            jump.save()
    """

def update_dop_link(request):
    data = {}
    if 'short' in request.POST:
        url = Link.objects.get(slug=request.POST['short'])
        #req = urllib2.Request(url.absolute_url, headers=hdr)
        urls = urllib2.build_opener(urllib2.HTTPCookieProcessor).open(url.absolute_url)

        try:
            hostname = urlparse(url.absolute_url).hostname
            scheme =  urlparse(url.absolute_url).scheme
        except:
            hostname = None
            scheme =  None
        tree = lxml.html.parse(urls).getroot()
        tiles = tree.find(".//title")
        if tiles:
            title = tree.find(".//title").text
        else:
            try:
               title = tree.cssselect('head title')[0].text
            except:
                title = ''
        zet = tree.cssselect('a')

        links = []
        for z in zet:
            #urllib2.build_opener(urllib2.HTTPCookieProcessor).open('http/www.championat.com/football/news-2086136-ibragimovich-povtoril-rekord-davidsa-po-chislu-krasnykh-kartochek-v-lige-chempionov.html')
            print(z.get('href'))
            try:
               sss = str(scheme) +'://' + str(hostname) + str(z.get('href'))
               #lins = urllib2.build_opener(urllib2.HTTPCookieProcessor).open(sss)
               #trees = lxml.html.parse(lins).getroot()
               #print(.text)
               links.append(sss)
            except:
               pass
        print(777, links)

        for lin in links:
            try:
                 lins = urllib2.build_opener(urllib2.HTTPCookieProcessor).open(lin)
                 tree = lxml.html.parse(lins).getroot()
                 tiles = tree.find(".//title")
                 if tiles:
                      title = tree.find(".//title").text
                 else:
                      try:
                           title = tree.cssselect('head title')[0].text
                      except:
                           title = ''
                 description = ''.join(tree.xpath("/html/head/meta[@name='description']/@content"))
                 keywords = ''.join(tree.xpath("/html/head/meta[@name='keywords']/@content"))
                 image = ''.join(tree.xpath("/html/head/meta[@property='og:image']/@content"))
                 favicon = ''.join(tree.xpath('//link[@rel="shortcut icon"]/@href'))
                 if title and description:
                   if len(title) > 22 and len(description) > 38:
                     short = _createLink()
                     usr = url.user
                     if not usr:
                         usr= None
                     respo = fast_url_parser(lin, short)
                     s = DopLink.objects.filter(slug=short).update(seo_title=title,user=usr,dop_link=True,
                       seo_description=description, seo_keywords=keywords, seo_image=image)
                     res = DopLink.objects.get(slug=short)
                     jumps = JumpLink.objects.get(link=url)
                     jumps.dop_links.add(res)
                     jumps.save()
            except:
                 pass
    response = HttpResponse(json.dumps({'data': data}), content_type="application/json")
    return response

#Быстрое создание parser DopLink
def fast_url_parser(urly, short):
    try:
        obj = DopLink.objects.create(site_name='', site='', slug=short, absolute_url=urly, dop_link=True)
        id = obj.id
        return True, id
    except:
        return fast_url_parser(urly, short)

#Быстрое создание parser Link
def fast_url_dot(urly, short):
    try:
        obj = Link.objects.create(site_name='', site='', slug=short, absolute_url=urly, dop_link=True)
        return obj.id
    except:
        return fast_url_dot(urly, short)