# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0004_auto_20150222_0239'),
    ]

    operations = [
        migrations.AddField(
            model_name='link',
            name='created',
            field=models.DateTimeField(default=datetime.datetime(2015, 2, 22, 17, 34, 20, 167000, tzinfo=utc), auto_now_add=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='link',
            name='updated',
            field=models.DateTimeField(default=datetime.datetime(2015, 2, 22, 17, 34, 39, 951000, tzinfo=utc), auto_now=True),
            preserve_default=False,
        ),
    ]
